package com.example.sucurity.dto;

import com.example.sucurity.user.Role;
import lombok.Data;
import lombok.ToString;

import java.util.UUID;

@Data
@ToString
public class UserDTO {

    private UUID id;
    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private Role role;
}
