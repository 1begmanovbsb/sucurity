package com.example.sucurity.exceptions;

public class UserShowException extends CustomException {
    public UserShowException(String title, String... var1) {
        super(title, var1);
    }

}
