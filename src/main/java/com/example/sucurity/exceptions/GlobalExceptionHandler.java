package com.example.sucurity.exceptions;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

  private static final String EXCEPTION = "Exception: ";
  private static final String DEFAULT_EXCEPTION = "Внутренняя ошибка сервиса, обратитесь к администратору";

  @ExceptionHandler(UserShowException.class)
  protected ResponseEntity<ApiError> handleNotFoundException(UserShowException ex) {
    log.error(EXCEPTION, ex);
    return buildResponseEntity(new ApiError(ex.getTitle(), HttpStatus.FORBIDDEN));
  }

  @ExceptionHandler(MaxUploadSizeExceededException.class)
  protected ResponseEntity<ApiError> handleSizeLimitExceededException(
      MaxUploadSizeExceededException ex) {
    log.error(EXCEPTION, ex);
    return buildResponseEntity(new ApiError(ex.getMessage(), HttpStatus.PAYLOAD_TOO_LARGE));
  }

  @ExceptionHandler(EmptyResultDataAccessException.class)
  protected ResponseEntity<ApiError> handleEmptyResultDataAccessException(
      EmptyResultDataAccessException ex) {
    log.error(EXCEPTION, ex);
    return buildResponseEntity(new ApiError(ex.getMessage(), HttpStatus.NOT_FOUND));
  }

  // Agreed to return 403 only in permission checking
  @ExceptionHandler(AccessDeniedException.class)
  protected ResponseEntity<ApiError> handleAccessDeniedException(AccessDeniedException ex) {
    log.info(ex.getClass().getName() + " {}", ex.getMessage());
    return buildResponseEntity(new ApiError(ex.getMessage(), HttpStatus.FORBIDDEN));
  }

  private ResponseEntity<ApiError> buildResponseEntity(ApiError apiError) {
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  protected ResponseEntity<ApiError> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
    log.info(ex.getClass().getName() + " {}", ex.getMessage());
    var message = ex.getFieldErrors()
            .stream()
            .map(fieldError -> fieldError.getField() + ": " + fieldError.getDefaultMessage())
            .collect(Collectors.joining("; "));
    return buildResponseEntity(new ApiError(ex.getBody().getTitle(), message, HttpStatus.UNPROCESSABLE_ENTITY));
  }

  @ExceptionHandler(Exception.class)
  protected ResponseEntity<ApiError> handleException(Exception ex) {
    log.error(EXCEPTION, ex);
    return buildResponseEntity(new ApiError(DEFAULT_EXCEPTION, HttpStatus.INTERNAL_SERVER_ERROR));
  }

  @Data
  @AllArgsConstructor
  @Schema(description = "Объект для представления ошибок системы")
  public static class ApiError {

    public ApiError(String error, HttpStatus status) {
      this.error = error;
      this.status = status;
    }

    @Schema(description = "Технический текст ошибки", maxLength = 65535)
    private String error;

    @Schema(description = "Сообщение об ошибке для клиента", maxLength = 65535)
    private String message;

    @Schema(description = "HTTP статус ошибки")
    private HttpStatus status;
  }
}
