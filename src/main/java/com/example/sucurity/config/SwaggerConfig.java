package com.example.sucurity.config;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@OpenAPIDefinition(info = @io.swagger.v3.oas.annotations.info.Info(
        title = "BAM Service",
        description = "Begmanov Adil Service",
        contact = @Contact(
                name = "Admin",
                email = "1begmanov@mail.ru")))
public class
SwaggerConfig {
    @Value("LOCAL")
    private String description;
    @Bean
    public GroupedOpenApi apiV1() {
        return GroupedOpenApi.builder()
                .group("v1 - First Version of API")
                .pathsToMatch(new String[]{"/api/v1/**", "/api/dict/**", "/api/v2/**"})
                .build();
    }

    @Bean
    public OpenAPI openApi() {
        final String securitySchemeName = "Authorization";
        return new OpenAPI()
//                .info(new Info()
//                        .title("BAM-rest-v1")
//                        .description("Сервис для разработки")
//                        .version("1.0.0")
//                        .contact(new io.swagger.v3.oas.models.info.Contact()
//                                .name("Бегманов Адиль")
//                                .url("https://mchd.ru")
//                                .email("m.korenev@opendgp.ru"))
//                        .termsOfService("CodeForensics")
//                        .license(new License().name("License").url("#"))
//                )
                .servers(List.of(new Server().url("/").description(description)))
                .components(
                        new Components()
                                .addSecuritySchemes(securitySchemeName,
                                        new SecurityScheme()
                                                .type(SecurityScheme.Type.APIKEY)
                                                .in(SecurityScheme.In.HEADER)
                                                .name("Authorization")
                                )
                )
                .security(List.of(new SecurityRequirement().addList(securitySchemeName)));
    }

    @Bean
    public GroupedOpenApi apiV2() {
        return GroupedOpenApi.builder()
                .group("v2 - TBD")
                .pathsToMatch(new String[]{"/api/dict/**", "/api/v2/**"})
                .build();
    }
}
