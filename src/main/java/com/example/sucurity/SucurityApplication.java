package com.example.sucurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SucurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SucurityApplication.class, args);
    }

}
