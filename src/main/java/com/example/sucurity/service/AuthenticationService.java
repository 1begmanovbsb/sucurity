//AuthenticationService.java
package com.example.sucurity.service;

import com.example.sucurity.AuthenticationRequest;
import com.example.sucurity.AuthenticationResponse;
import com.example.sucurity.RegisterRequest;
import com.example.sucurity.service.JwtService;
import com.example.sucurity.exceptions.UserShowException;
import com.example.sucurity.user.Role;
import com.example.sucurity.user.User;
import com.example.sucurity.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    @Autowired
    MessageSource messageSource;

    public AuthenticationResponse register(RegisterRequest request) {
        Optional<User> emailValidate = repository.findByEmail(request.getEmail());
        if (!emailValidate.isEmpty()) {
            throw new UserShowException(getLocalizedExceptionMessage("emailValidate"));
        }
        var user = User.builder()
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.User)
                .build();
        repository.save(user);
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = repository.findByEmail(request.getEmail())
                .orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

    private String getLocalizedExceptionMessage(String key, String... args) {
        Locale locale = LocaleContextHolder.getLocaleContext().getLocale();
        String message = messageSource.getMessage(key, args, locale);
        return message;
    }


}
