//DemoController.java
package com.example.sucurity.controllers;

import com.example.sucurity.exceptions.UserShowException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
@RequestMapping("/api/v1/Exception")
public class DemoController {
    @GetMapping
    public ResponseEntity<String> sayHello() {
        return ResponseEntity.ok("Hello from secured endpoint");
    }

    protected <T> ResponseEntity<T> buildResponse(T result) {
        if (result == null) {
            throw new RuntimeException("file.result_null");
        }
        return ResponseEntity.ok(result);
    }
}



