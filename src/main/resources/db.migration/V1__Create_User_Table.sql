create table if not exists users
(
    id uuid,
    email     varchar(255),
    firstname varchar(255),
    lastname  varchar(255),
    password  varchar(255),
    role      varchar(255) check (role in ('User', 'Admin'))
);

create sequence if not exists users_seq;
